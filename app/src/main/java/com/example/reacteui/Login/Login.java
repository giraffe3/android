//package com.example.reacteui.Login;
//
//
//
//import android.content.Intent;
//import android.database.Cursor;
//import android.os.Bundle;
//import android.os.Handler;
//import android.os.Looper;
//import android.util.Log;
//import android.view.View;
//import android.widget.EditText;
//import android.widget.Toast;
//
//import androidx.activity.EdgeToEdge;
//import androidx.appcompat.app.AppCompatActivity;
//import androidx.core.graphics.Insets;
//import androidx.core.view.ViewCompat;
//import androidx.core.view.WindowInsetsCompat;
//
//import com.example.reacteui.MainActivity;
//import com.example.reacteui.R;
//import com.example.reacteui.SQL.MyDatabaseHelper;
//import com.example.reacteui.register.Register;
//
//public class Login extends AppCompatActivity implements View.OnClickListener {
//
//    MyDatabaseHelper myDb;
//    Handler mainHandler;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        EdgeToEdge.enable(this);
//        setContentView(R.layout.activity_login);
//        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
//            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
//            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
//            return insets;
//        });
//        findViewById(R.id.toRegister).setOnClickListener(this);
//        findViewById(R.id.login).setOnClickListener(this);
//        myDb = new MyDatabaseHelper(this);
//        mainHandler = new Handler(Looper.getMainLooper());
//    }
//
//    @Override
//    public void onClick(View v) {
//        switch (v.getId()) {
//            case R.id.login:
//                EditText usernameText = findViewById(R.id.username_login);
//                EditText passwordText = findViewById(R.id.password_login);
//                String username = usernameText.getText().toString();
//                String password = passwordText.getText().toString();
//                Cursor res = myDb.getDataByUsername(username);
//                if (res.getCount() == 0) {
//                    Toast.makeText(this, "用户名不存在", Toast.LENGTH_SHORT).show();
//                } else {
//                    int passwordIndex = res.getColumnIndex(MyDatabaseHelper.COLUMN_PASSWORD);
//                    if (passwordIndex == -1) {
//                        Log.e("Login", "Column " + MyDatabaseHelper.COLUMN_PASSWORD + " not found in the database");
//                        Toast.makeText(this, "数据库中缺少密码列", Toast.LENGTH_SHORT).show();
//                    } else {
//                        while (res.moveToNext()) {
//                            String dbPassword = res.getString(passwordIndex);
//                            if (password.equals(dbPassword)) {
//                                Toast.makeText(this, "登录成功", Toast.LENGTH_SHORT).show();
//                                Intent intent = new Intent(this, MainActivity.class);
//                                startActivity(intent);
//                                return; // 登录成功后跳出方法
//                            }
//                        }
//                        Toast.makeText(this, "密码错误", Toast.LENGTH_SHORT).show();
//                    }
//                }
//                if (res != null) {
//                    res.close(); // 关闭Cursor
//                }
//                break;
//
//            case R.id.toRegister:
//                Intent intent = new Intent(this, Register.class);
//                startActivity(intent);
//                break;
//        }
//    }
//
//}
//


package com.example.reacteui.Login;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

import com.example.reacteui.MainActivity;
import com.example.reacteui.R;
import com.example.reacteui.SQL.MyDatabaseHelper;
import com.example.reacteui.register.Register;

public class Login extends AppCompatActivity implements View.OnClickListener {

    MyDatabaseHelper myDb;
    Handler mainHandler;
    EditText usernameText;
    EditText passwordText;
    CheckBox rememberPasswordCheckbox;
    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_login);

        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });

        usernameText = findViewById(R.id.username_login);
        passwordText = findViewById(R.id.password_login);
        rememberPasswordCheckbox = findViewById(R.id.remember_password);

        findViewById(R.id.toRegister).setOnClickListener(this);
        findViewById(R.id.login).setOnClickListener(this);

        myDb = new MyDatabaseHelper(this);
        mainHandler = new Handler(Looper.getMainLooper());

        // 初始化 SharedPreferences
        sharedPreferences = getSharedPreferences("UserPrefs", MODE_PRIVATE);

        // 读取保存的用户名和密码
        String savedUsername = sharedPreferences.getString("username", "");
        String savedPassword = sharedPreferences.getString("password", "");
        boolean isRemembered = sharedPreferences.getBoolean("remember_password", false);

        if (isRemembered) {
            usernameText.setText(savedUsername);
            passwordText.setText(savedPassword);
            rememberPasswordCheckbox.setChecked(true);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.login:
                String username = usernameText.getText().toString();
                String password = passwordText.getText().toString();
                Cursor res = myDb.getDataByUsername(username);
                if (res.getCount() == 0) {
                    Toast.makeText(this, "用户名不存在", Toast.LENGTH_SHORT).show();
                } else {
                    int passwordIndex = res.getColumnIndex(MyDatabaseHelper.COLUMN_PASSWORD);
                    if (passwordIndex == -1) {
                        Log.e("Login", "Column " + MyDatabaseHelper.COLUMN_PASSWORD + " not found in the database");
                        Toast.makeText(this, "用户名或密码错误", Toast.LENGTH_SHORT).show();
                    } else {
                        while (res.moveToNext()) {
                            String dbPassword = res.getString(passwordIndex);
                            if (password.equals(dbPassword)) {
                                Toast.makeText(this, "登录成功", Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(this, MainActivity.class);
                                startActivity(intent);

                                // 保存用户名和密码
                                SharedPreferences.Editor editor = sharedPreferences.edit();
                                editor.putString("username", username);
                                editor.putString("password", password);
                                editor.putBoolean("remember_password", rememberPasswordCheckbox.isChecked());
                                editor.apply();

                                return; // 登录成功后跳出方法
                            }
                        }
                        Toast.makeText(this, "密码错误", Toast.LENGTH_SHORT).show();
                    }
                }
                if (res != null) {
                    res.close(); // 关闭Cursor
                }
                break;

            case R.id.toRegister:
                Intent intent = new Intent(this, Register.class);
                startActivity(intent);
                break;
        }
    }
}
