// MerchantDetailFragment.java
package com.example.reacteui.Merchant;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.reacteui.Merchant.ProductAdapter;
import com.example.reacteui.Merchant.Merchant;
import com.example.reacteui.Merchant.Product;
import com.example.reacteui.R;

import java.util.ArrayList;
import java.util.List;

public class MerchantDetailFragment extends Fragment implements ProductAdapter.OnItemClickListener, ProductAdapter.OnAddToCartClickListener {

    private Merchant merchant;
    private RecyclerView recyclerViewProducts;
    private ProductAdapter productAdapter;
    private List<Product> productList;

    public MerchantDetailFragment(Merchant merchant) {
        this.merchant = merchant;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_merchant_detail, container, false);

        TextView merchantName = view.findViewById(R.id.merchant_name_detail);
        ImageView merchantBackground = view.findViewById(R.id.merchant_background);
        TextView merchantDescription = view.findViewById(R.id.merchant_description_detail);
        recyclerViewProducts = view.findViewById(R.id.recycler_view_products);

        merchantName.setText(merchant.getName());
        merchantDescription.setText(merchant.getDescription());

        // Load products from SharedPreferences
        productList = loadProductsFromSharedPreferences();
        productAdapter = new ProductAdapter(productList, this, this);
        recyclerViewProducts.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerViewProducts.setAdapter(productAdapter);

        return view;
    }

    private List<Product> loadProductsFromSharedPreferences() {
        // Implement loading products from SharedPreferences
        // For demonstration, returning a hardcoded list
        List<Product> products = new ArrayList<>();
        products.add(new Product("商品A", 19.99, "这是商品A的描述",""));
        products.add(new Product("商品B", 29.99, "这是商品B的描述",""));
        return products;
    }

    @Override
    public void onItemClick(Product product) {
        // Handle item click event
    }

    @Override
    public void onAddToCartClick(Product product) {
        // Add product to cart in SharedPreferences
        // Implement saving to SharedPreferences
    }


}
