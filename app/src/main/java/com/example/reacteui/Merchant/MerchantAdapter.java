package com.example.reacteui.Merchant;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.reacteui.R;

import java.util.List;

public class MerchantAdapter extends RecyclerView.Adapter<MerchantAdapter.MerchantViewHolder> {

    private List<Merchant> merchantList;
    private Context context;
    private OnItemClickListener listener;

    public interface OnItemClickListener {
        void onItemClick(Merchant merchant);
    }

    public MerchantAdapter(List<Merchant> merchantList, Context context, OnItemClickListener listener) {
        this.merchantList = merchantList;
        this.context = context;
        this.listener = listener;
    }

    @NonNull
    @Override
    public MerchantViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.merchant_list_item, parent, false);
        return new MerchantViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MerchantViewHolder holder, int position) {
        Merchant merchant = merchantList.get(position);
        holder.bind(merchant);
    }

    @Override
    public int getItemCount() {
        return merchantList.size();
    }

    public void filterList(List<Merchant> filteredList) {
        merchantList = filteredList;
        notifyDataSetChanged();
    }

    public class MerchantViewHolder extends RecyclerView.ViewHolder {

        private ImageView merchantImage;
        private TextView merchantName;
        private TextView merchantDescription;
        private RecyclerView productList;
        private Button addToCartButton;

        public MerchantViewHolder(@NonNull View itemView) {
            super(itemView);
            merchantImage = itemView.findViewById(R.id.merchant_image);
            merchantName = itemView.findViewById(R.id.merchant_name);
            merchantDescription = itemView.findViewById(R.id.merchant_description);
            productList = itemView.findViewById(R.id.product_list);
            addToCartButton = itemView.findViewById(R.id.add_to_cart_button);

            itemView.setOnClickListener(v -> {
                if (listener != null) {
                    listener.onItemClick(merchantList.get(getAdapterPosition()));
                }
            });

            addToCartButton.setOnClickListener(v -> {
                Toast.makeText(context, "添加到购物车", Toast.LENGTH_SHORT).show();
            });
        }

        public void bind(Merchant merchant) {
            Glide.with(context).load(merchant.getImageUrl()).into(merchantImage);
            merchantName.setText(merchant.getName());
            merchantDescription.setText(merchant.getDescription());

            // 设置商品列表
            ProductAdapter productAdapter = new ProductAdapter(
                    merchant.getProducts(),
                    new ProductAdapter.OnItemClickListener() {
                        @Override
                        public void onItemClick(Product product) {
                            // 处理商品点击事件
                            Toast.makeText(context, "商品点击: " + product.getName(), Toast.LENGTH_SHORT).show();
                        }
                    },
                    new ProductAdapter.OnAddToCartClickListener() {
                        @Override
                        public void onAddToCartClick(Product product) {
                            // 处理添加到购物车事件
                            Toast.makeText(context, "添加到购物车: " + product.getName(), Toast.LENGTH_SHORT).show();
                        }
                    }
            );
            productList.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
            productList.setAdapter(productAdapter);
        }
    }
}
   