// MerchantDetailActivity.java
package com.example.reacteui.Merchant;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import com.example.reacteui.R;

import java.util.ArrayList;
import java.util.List;

public class MerchantDetailActivity extends AppCompatActivity implements ProductAdapter.OnItemClickListener, ProductAdapter.OnAddToCartClickListener {

    private ImageView merchantImage;
    private TextView merchantName, merchantDescription;
    private RecyclerView productList;
    private ProductAdapter productAdapter;
    private List<Product> productListData;
    private Button checkoutButton; // 声明 checkoutButton

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_merchant_detail);

        merchantImage = findViewById(R.id.merchant_image);
        merchantName = findViewById(R.id.merchant_name);
        merchantDescription = findViewById(R.id.merchant_description);
        productList = findViewById(R.id.product_list);
        checkoutButton = findViewById(R.id.checkout_button); // 初始化 checkoutButton

        // 获取传递过来的商户对象
        Merchant merchant = getIntent().getParcelableExtra("merchant");

        if (merchant != null) {
            // 设置商户信息
            merchantName.setText(merchant.getName());
            merchantDescription.setText(merchant.getDescription());
            Glide.with(this)
                    .load(merchant.getImageUrl())
                    .into(merchantImage);

            // 使用实际的产品数据
            productListData = merchant.getProducts();

            // 设置 RecyclerView
            productAdapter = new ProductAdapter(productListData, this, this);
            productList.setLayoutManager(new LinearLayoutManager(this));
            productList.setAdapter(productAdapter);
        } else {
            Toast.makeText(this, "商户信息获取失败", Toast.LENGTH_SHORT).show();
        }

        // 处理 checkout 按钮点击事件
        checkoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CartFragment cartFragment = new CartFragment();
                cartFragment.show(getSupportFragmentManager(), "cart_fragment");
            }
        });
    }

    @Override
    public void onItemClick(Product product) {
        // 处理商品点击事件
        Toast.makeText(this, "点击了商品: " + product.getName(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onAddToCartClick(Product product) {
        // 添加商品到购物车
        Cart cart = Cart.getInstance(); // 假设 Cart 是单例模式
        cart.addProduct(product);
        Toast.makeText(this, product.getName() + " 已添加到购物车", Toast.LENGTH_SHORT).show();
    }
}
