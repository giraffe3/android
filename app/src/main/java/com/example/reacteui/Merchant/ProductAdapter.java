//// ProductAdapter.java
//package com.example.reacteui.Merchant;
//
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.Button;
//import android.widget.TextView;
//
//import androidx.annotation.NonNull;
//import androidx.recyclerview.widget.RecyclerView;
//
//import com.example.reacteui.R;
//
//import java.util.List;
//
////public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ProductViewHolder> {
////
////    private List<Product> productList;
////    private OnItemClickListener listener;
////
////    public interface OnItemClickListener {
////        void onItemClick(Product product);
////    }
////
////    // 移除 OnItemClickListener 参数
////    public ProductAdapter(List<Product> productList) {
////        this.productList = productList;
////        this.listener = null; // 或者提供一个默认的实现
////    }
////
////    // 如果需要默认的 OnItemClickListener 实现，可以这样写
////    public ProductAdapter(List<Product> productList, OnItemClickListener listener) {
////        this.productList = productList;
////        this.listener = listener;
////    }
////
////    @NonNull
////    @Override
////    public ProductViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
////        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_product, parent, false);
////        return new ProductViewHolder(itemView);
////    }
////
////    @Override
////    public void onBindViewHolder(@NonNull ProductViewHolder holder, int position) {
////        Product product = productList.get(position);
////        holder.productName.setText(product.getName());
////        holder.itemView.setOnClickListener(v -> listener.onItemClick(product));
////    }
////
////    @Override
////    public int getItemCount() {
////        return productList.size();
////    }
////
////    static class ProductViewHolder extends RecyclerView.ViewHolder {
////        TextView productName;
////
////        public ProductViewHolder(@NonNull View itemView) {
////            super(itemView);
////            productName = itemView.findViewById(R.id.product_name);
////        }
////    }
////
////
////}
////
//
//// ProductAdapter.java
//public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ProductViewHolder> {
//
//    private List<Product> productList;
//    private MerchantAdapter.OnItemClickListener listener;
//    private OnAddToCartClickListener addToCartListener;
//
//    public interface OnAddToCartClickListener {
//        void onAddToCartClick(Product product);
//    }
//
//    public ProductAdapter(List<Product> productList, MerchantAdapter.OnItemClickListener listener, OnAddToCartClickListener addToCartListener) {
//        this.productList = productList;
//        this.listener = listener;
//        this.addToCartListener = addToCartListener;
//    }
//
//    @NonNull
//    @Override
//    public ProductViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
//        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_product, parent, false);
//        return new ProductViewHolder(itemView);
//    }
//
//    @Override
//    public void onBindViewHolder(@NonNull ProductViewHolder holder, int position) {
//        Product product = productList.get(position);
//        holder.productName.setText(product.getName());
//        holder.itemView.setOnClickListener(v -> listener.onItemClick(product));
//        holder.addToCartButton.setOnClickListener(v -> addToCartListener.onAddToCartClick(product));
//    }
//
//    @Override
//    public int getItemCount() {
//        return productList.size();
//    }
//
//    static class ProductViewHolder extends RecyclerView.ViewHolder {
//        TextView productName;
//        Button addToCartButton;
//
//        public ProductViewHolder(@NonNull View itemView) {
//            super(itemView);
//            productName = itemView.findViewById(R.id.product_name);
//            addToCartButton = itemView.findViewById(R.id.add_to_cart_button);
//        }
//    }
//}
// ProductAdapter.java
package com.example.reacteui.Merchant;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.reacteui.R;

import java.util.List;

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ProductViewHolder> {

    private List<Product> productList;
    private OnItemClickListener listener;
    private OnAddToCartClickListener addToCartListener;

    public interface OnItemClickListener {
        void onItemClick(Product product);
    }

    public interface OnAddToCartClickListener {
        void onAddToCartClick(Product product);
    }

    public ProductAdapter(List<Product> productList, OnItemClickListener listener, OnAddToCartClickListener addToCartListener) {
        this.productList = productList;
        this.listener = listener;
        this.addToCartListener = addToCartListener;
    }

    @NonNull
    @Override
    public ProductViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_product, parent, false);
        return new ProductViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductViewHolder holder, int position) {
        Product product = productList.get(position);
        holder.productName.setText(product.getName());
        holder.itemView.setOnClickListener(v -> listener.onItemClick(product));
        holder.addToCartButton.setOnClickListener(v -> addToCartListener.onAddToCartClick(product));
    }

    @Override
    public int getItemCount() {
        return productList.size();
    }

    public interface OnAddToCartListener {
    }

    static class ProductViewHolder extends RecyclerView.ViewHolder {
        TextView productName;
        Button addToCartButton;

        public ProductViewHolder(@NonNull View itemView) {
            super(itemView);
            productName = itemView.findViewById(R.id.product_name);
            addToCartButton = itemView.findViewById(R.id.add_to_cart_button);
        }
    }
}
