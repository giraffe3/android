// Cart.java
package com.example.reacteui.Merchant;

import java.util.ArrayList;
import java.util.List;

public class Cart {
    private static Cart instance;
    private List<Product> cartList;

    private Cart() {
        cartList = new ArrayList<>();
    }

    public static synchronized Cart getInstance() {
        if (instance == null) {
            instance = new Cart();
        }
        return instance;
    }

    public void addProduct(Product product) {
        cartList.add(product);
    }

    public void removeProduct(Product product) {
        cartList.remove(product);
    }

    public List<Product> getCartList() {
        return cartList;
    }

    public double getTotalPrice() {
        double totalPrice = 0;
        for (Product product : cartList) {
            totalPrice += product.getPrice();
        }
        return totalPrice;
    }
}
