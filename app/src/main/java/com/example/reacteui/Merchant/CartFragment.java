package com.example.reacteui.Merchant;//package com.example.reacteui.Merchant;
//
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.reacteui.MainActivity;
import com.example.reacteui.Merchant.CartAdapter;
import com.example.reacteui.Merchant.Product;
import com.example.reacteui.R;


import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
//
//public class CartFragment extends DialogFragment {
//
//    private RecyclerView recyclerViewCart;
//    private CartAdapter cartAdapter;
//    private List<Product> cartList;
//
//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container,
//                             Bundle savedInstanceState) {
//        View view = inflater.inflate(R.layout.fragment_cart, container, false);
//
//        recyclerViewCart = view.findViewById(R.id.recycler_view_cart);
//        Button settleButton = view.findViewById(R.id.settle_button);
//
//        cartList = loadCartFromSharedPreferences();
//        cartAdapter = new CartAdapter(cartList);
//        recyclerViewCart.setLayoutManager(new LinearLayoutManager(getContext()));
//        recyclerViewCart.setAdapter(cartAdapter);
//
//        settleButton.setOnClickListener(v -> settleOrder());
//
//        return view;
//    }
//
//    private List<Product> loadCartFromSharedPreferences() {
//        // Implement loading cart from SharedPreferences
//        // For demonstration, returning a hardcoded list
//        List<Product> cart = new ArrayList<>();
//        cart.add(new Product("商品A", 19.99, "这是商品A的描述",""));
//        cart.add(new Product("商品B", 29.99, "这是商品B的描述",""));
//        return cart;
//    }
//
//    private void settleOrder() {
//        // Check address and phone number
//        String address = "输入的地址"; // Replace with actual input
//        String phoneNumber = "输入的手机号码"; // Replace with actual input
//
//        if (address.isEmpty() || phoneNumber.isEmpty()) {
//            Toast.makeText(getContext(), "请输入地址和手机号码", Toast.LENGTH_SHORT).show();
//        } else {
//            // Calculate total price
//            double totalPrice = 0;
//            for (Product product : cartList) {
//                totalPrice += product.getPrice();
//            }
//
//            // Display payment completion message
//            Toast.makeText(getContext(), "支付完成，总价: $" + totalPrice, Toast.LENGTH_SHORT).show();
//
//            // Dismiss the dialog and return to merchant detail page
//            dismiss();
//        }
//    }
//}




// CartFragment.java


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.reacteui.MainActivity;
import com.example.reacteui.R;

import java.util.List;

// CartFragment.java
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.reacteui.MainActivity;
import com.example.reacteui.R;

import java.util.List;

//public class CartFragment extends DialogFragment {
//
//    private Cart cart;
//    private RecyclerView recyclerViewCart;
//    private CartAdapter cartAdapter;
//    private List<Product> cartList;
//
//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container,
//                             Bundle savedInstanceState) {
//        View view = inflater.inflate(R.layout.fragment_cart, container, false);
//
//        recyclerViewCart = view.findViewById(R.id.recycler_view_cart);
//        Button settleButton = view.findViewById(R.id.settle_button);
//        EditText addressEditText = view.findViewById(R.id.address_edit_text);
//        EditText phoneNumberEditText = view.findViewById(R.id.phone_number_edit_text);
//        TextView totalPriceTextView = view.findViewById(R.id.total_price_text_view);
//
//        cart = new Cart();
//        cartList = cart.getCartList();
//        cartAdapter = new CartAdapter(cartList);
//        recyclerViewCart.setLayoutManager(new LinearLayoutManager(getContext()));
//        recyclerViewCart.setAdapter(cartAdapter);
//
//        totalPriceTextView.setText("总价: $" + cart.getTotalPrice());
//
//        settleButton.setOnClickListener(v -> settleOrder(addressEditText.getText().toString(), phoneNumberEditText.getText().toString()));
//
//        return view;
//    }
//
//    private void settleOrder(String address, String phoneNumber) {
//        if (address.isEmpty() || phoneNumber.isEmpty()) {
//            Toast.makeText(getContext(), "请输入地址和手机号码", Toast.LENGTH_SHORT).show();
//        } else {
//            double totalPrice = cart.getTotalPrice();
//            Toast.makeText(getContext(), "支付完成，总价: $" + totalPrice, Toast.LENGTH_SHORT).show();
//            dismiss();
//            navigateToHomePage();
//        }
//    }
//
//    private void navigateToHomePage() {
//        // 实现导航到主页的逻辑
//        // 例如：启动一个新的 Activity 或 Fragment
//         requireActivity().startActivity(new Intent(requireActivity(), MainActivity.class));
//    }
//}
// CartFragment.java


import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.reacteui.MainActivity;
import com.example.reacteui.R;

public class CartFragment extends DialogFragment {

    private Cart cart;
    private RecyclerView recyclerViewCart;
    private CartAdapter cartAdapter;
    private List<Product> cartList;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_cart, container, false);

        recyclerViewCart = view.findViewById(R.id.recycler_view_cart);
        Button settleButton = view.findViewById(R.id.settle_button);
        EditText addressEditText = view.findViewById(R.id.address_edit_text);
        EditText phoneNumberEditText = view.findViewById(R.id.phone_number_edit_text);
        TextView totalPriceTextView = view.findViewById(R.id.total_price_text_view);

        cart = Cart.getInstance(); // 假设 Cart 是单例模式
        cartList = cart.getCartList();
        cartAdapter = new CartAdapter(cartList);
        recyclerViewCart.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerViewCart.setAdapter(cartAdapter);

        totalPriceTextView.setText("总价: $" + cart.getTotalPrice());

        settleButton.setOnClickListener(v -> settleOrder(addressEditText.getText().toString(), phoneNumberEditText.getText().toString()));

        return view;
    }

    private void settleOrder(String address, String phoneNumber) {
        if (address.isEmpty() || phoneNumber.isEmpty()) {
            Toast.makeText(getContext(), "请输入地址和手机号码", Toast.LENGTH_SHORT).show();
        } else {
            double totalPrice = cart.getTotalPrice();
            Toast.makeText(getContext(), "支付完成，总价: $" + totalPrice, Toast.LENGTH_SHORT).show();
            dismiss();
            navigateToHomePage();
        }
    }

    private void navigateToHomePage() {
        // 实现导航到主页的逻辑
        requireActivity().startActivity(new Intent(requireActivity(), MainActivity.class));
    }
}


