package com.example.reacteui;

import android.os.Bundle;
import androidx.activity.EdgeToEdge;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;
import androidx.fragment.app.Fragment;
import androidx.viewpager2.widget.ViewPager2;

import com.example.reacteui.Fragment.FragmentHomePage;
import com.example.reacteui.Fragment.FragmentDiscoverPage;
import com.example.reacteui.Fragment.FragmentUserPage;
import com.example.reacteui.ViewPageAdapter.ViewPageAdapter;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private ViewPager2 viewPager;
    private ViewPageAdapter viewPageAdapter;
    private List<Fragment> fragmentList;
    private BottomNavigationView bottomNavigationView;
    private SearchView searchView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_main);
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });
        viewPager = findViewById(R.id.viewPager);
        bottomNavigationView = findViewById(R.id.botNavLayout);
        searchView = findViewById(R.id.searchView);
        initData();
        viewPageAdapter = new ViewPageAdapter(getSupportFragmentManager(), getLifecycle(), fragmentList);
        viewPager.setAdapter(viewPageAdapter);

        // 设置 BottomNavigationView 的监听器
        bottomNavigationView.setOnNavigationItemSelectedListener(item -> {
            switch (item.getItemId()) {
                case R.id.menu_home:
                    viewPager.setCurrentItem(0);
                    return true;
                case R.id.menu_find:
                    viewPager.setCurrentItem(1);
                    return true;
                case R.id.menu_mine:
                    viewPager.setCurrentItem(2);
                    return true;
            }
            return false;
        });

        // 同步 ViewPager2 和 BottomNavigationView 的状态
        viewPager.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                switch (position) {
                    case 0:
                        bottomNavigationView.setSelectedItemId(R.id.menu_home);
                        break;
                    case 1:
                        bottomNavigationView.setSelectedItemId(R.id.menu_find);
                        break;
                    case 2:
                        bottomNavigationView.setSelectedItemId(R.id.menu_mine);
                        break;
                }
            }
        });

        // 设置 SearchView 的监听器
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                handleSearch(query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                handleSearch(newText);
                return true;
            }
        });
    }

    private void initData() {
        fragmentList = new ArrayList<>();
        FragmentHomePage fragmentHomePage = FragmentHomePage.newInstance();
        fragmentList.add(fragmentHomePage);
        FragmentDiscoverPage fragmentSearchPage = FragmentDiscoverPage.newInstance();
        fragmentList.add(fragmentSearchPage);
        FragmentUserPage fragmentUserPage = FragmentUserPage.newInstance("", "");
        fragmentList.add(fragmentUserPage);
    }

    private void handleSearch(String query) {
        Fragment currentFragment = viewPageAdapter.createFragment(viewPager.getCurrentItem());
        if (currentFragment instanceof FragmentHomePage) {
            ((FragmentHomePage) currentFragment).filter(query);
        }
    }
}
