package com.example.reacteui.DiscoverActivity;

import com.example.reacteui.Merchant.Product;
import java.io.Serializable;
import java.util.List;
import java.util.ArrayList;

public class Merchant implements Serializable {
    private String name;
    private String description;
    private String imageUrl;
    private List<Product> productList; // 声明 productList 变量

    public Merchant(String name, String description, String imageUrl) {
        this.name = name;
        this.description = description;
        this.imageUrl = imageUrl;
        this.productList = new ArrayList<>(); // 初始化 productList
    }

    // Getters and setters
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public List<Product> getProductList() {
        return productList;
    }

    public void setProductList(List<Product> productList) {
        this.productList = productList;
    }
}
