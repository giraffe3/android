// MerchantDetailActivity.java
package com.example.reacteui.DiscoverActivity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.reacteui.Merchant.MerchantDetailFragment;
import com.example.reacteui.Merchant.Product;
import com.example.reacteui.Merchant.ProductAdapter;
import com.example.reacteui.R;
import com.example.reacteui.DiscoverActivity.Merchant;

import java.util.List;

public class MerchantDetailActivity extends AppCompatActivity implements ProductAdapter.OnItemClickListener, ProductAdapter.OnAddToCartClickListener {

    private ImageView imageViewMerchant;
    private TextView textViewName;
    private TextView textViewDescription;
    private RecyclerView recyclerViewProducts;
    private ProductAdapter productAdapter;

    @SuppressLint("MissingInflatedId")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_merchant_detail);

        // Initialize views
        imageViewMerchant = findViewById(R.id.image_view_merchant);
        textViewName = findViewById(R.id.text_view_name);
        textViewDescription = findViewById(R.id.text_view_description);
        recyclerViewProducts = findViewById(R.id.recycler_view_products);

        // Get merchant from intent
        Merchant merchant = getIntent().getParcelableExtra("merchant");

        // 从Intent中获取Merchant对象
        if (getIntent().hasExtra("merchant")) {
            merchant = (Merchant) getIntent().getSerializableExtra("merchant");
            Log.d("MerchantDetailActivity", "Received merchant: " + (merchant != null ? merchant.getName() : "null"));
            if (merchant != null) {
                // 显示商户信息
                displayMerchantInfo(merchant);
            } else {
                // 处理商户信息为空的情况
                Toast.makeText(this, "商户信息获取失败", Toast.LENGTH_SHORT).show();
            }
        } else {
            // 处理没有接收到商户信息的情况
            Toast.makeText(this, "商户信息获取失败", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onAddToCartClick(Product product) {
        // 处理添加到购物车的逻辑
    }

    @Override
    public void onItemClick(Product product) {
       
    }

    private void displayMerchantInfo(Merchant merchant) {
        // 显示商户信息的逻辑
        textViewName.setText(merchant.getName());
        textViewDescription.setText(merchant.getDescription());
        Glide.with(this).load(merchant.getImageUrl()).into(imageViewMerchant);
    }
}
