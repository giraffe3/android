package com.example.reacteui.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.example.reacteui.userPage.AddressesActivity;
import com.example.reacteui.userPage.CouponsActivity;
import com.example.reacteui.userPage.HelpActivity;
import com.example.reacteui.userPage.SettingsActivity;
import com.example.reacteui.R;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FragmentUserPage#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentUserPage extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public FragmentUserPage() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentUserPage.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentUserPage newInstance(String param1, String param2) {
        FragmentUserPage fragment = new FragmentUserPage();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_user_page, container, false);

        // 加载用户数据
        loadUserData(view);

        // 添加按钮点击事件监听器
        Button btnCoupons = view.findViewById(R.id.btn_coupons);
        btnCoupons.setOnClickListener(v -> {
            // 跳转到优惠券页面
            Intent intent = new Intent(getContext(), CouponsActivity.class);
            startActivity(intent);
        });

        Button btnAddresses = view.findViewById(R.id.btn_addresses);
        btnAddresses.setOnClickListener(v -> {
            // 跳转到地址管理页面
            Intent intent = new Intent(getContext(), AddressesActivity.class);
            startActivity(intent);
        });

        Button btnSettings = view.findViewById(R.id.btn_settings);
        btnSettings.setOnClickListener(v -> {
            // 跳转到设置页面
            Intent intent = new Intent(getContext(), SettingsActivity.class);
            startActivity(intent);
        });

        Button btnHelp = view.findViewById(R.id.btn_help);
        btnHelp.setOnClickListener(v -> {
            // 跳转到帮助页面
            Intent intent = new Intent(getContext(), HelpActivity.class);
            startActivity(intent);
        });

        return view;
    }

    // 在 FragmentUserPage 类中添加以下方法
    private void loadUserData(View view) {
        // 这里假设我们有一个方法从服务器获取用户数据
        UserData userData = fetchUserDataFromServer();

        // 更新 UI
        TextView userNameTextView = view.findViewById(R.id.user_name);
        userNameTextView.setText(userData.getName());

        ImageView userAvatarImageView = view.findViewById(R.id.user_avatar);
        // 设置用户头像，这里可以使用 Glide 或 Picasso 加载网络图片
        Glide.with(this).load(userData.getAvatarUrl()).into(userAvatarImageView);
    }

    // 模拟从服务器获取用户数据的方法
    private UserData fetchUserDataFromServer() {
        // 实际应用中，这里应该是网络请求
        return new UserData("张三", "https://ts2.cn.mm.bing.net/th?id=OIP-C.V_MhaUW-iKtxQw3SMUPOWgHaIV&w=235&h=265&c=8&rs=1&qlt=90&o=6&dpr=1.3&pid=3.1&rm=2");
    }

    // 创建一个简单的 UserData 类来存储用户信息
    class UserData {
        private String name;
        private String avatarUrl;

        public UserData(String name, String avatarUrl) {
            this.name = name;
            this.avatarUrl = avatarUrl;
        }

        public String getName() {
            return name;
        }

        public String getAvatarUrl() {
            return avatarUrl;
        }
    }
}
