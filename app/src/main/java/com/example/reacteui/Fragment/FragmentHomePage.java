package com.example.reacteui.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.reacteui.Merchant.Merchant;
import com.example.reacteui.Merchant.MerchantAdapter;
import com.example.reacteui.Merchant.MerchantDetailActivity;
import com.example.reacteui.Merchant.Product;
import com.example.reacteui.R;

import java.util.ArrayList;
import java.util.List;

public class FragmentHomePage extends Fragment implements MerchantAdapter.OnItemClickListener {

    private RecyclerView recyclerView;
    private MerchantAdapter merchantAdapter;
    private List<Merchant> merchantList;

    public FragmentHomePage() {
        // Required empty public constructor
    }

    public static FragmentHomePage newInstance() {
        FragmentHomePage fragment = new FragmentHomePage();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home_page, container, false);

        // 初始化RecyclerView
        recyclerView = view.findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setHasFixedSize(true);

        // 初始化商户数据列表
        merchantList = new ArrayList<>();

        // 创建产品列表
        List<Product> productsBeijingDuck = new ArrayList<>();
        productsBeijingDuck.add(new Product("烤鸭", 120.0, "传统的北京烤鸭", "https://img.icons8.com/doodle/48/beijing-duck.png"));
        productsBeijingDuck.add(new Product("酱肘子", 50.0, "北京特色酱肘子", "https://img.icons8.com/doodle/48/pig-foot.png"));
        productsBeijingDuck.add(new Product("炸酱面", 30.0, "经典的北京炸酱面", "https://img.icons8.com/doodle/48/noodles.png"));
        productsBeijingDuck.add(new Product("豆汁儿", 15.0, "老北京的传统饮品豆汁儿", "https://img.icons8.com/doodle/48/coffee-cup.png"));
        productsBeijingDuck.add(new Product("焦圈", 20.0, "酥脆的焦圈", "https://img.icons8.com/doodle/48/circle-snack.png"));
        productsBeijingDuck.add(new Product("艾窝窝", 25.0, "甜美的艾窝窝", "https://img.icons8.com/doodle/48/dessert.png"));
        productsBeijingDuck.add(new Product("驴打滚", 35.0, "软糯的驴打滚", "https://img.icons8.com/doodle/48/dessert.png"));
        productsBeijingDuck.add(new Product("糖火烧", 40.0, "香甜的糖火烧", "https://img.icons8.com/doodle/48/fire.png"));
        productsBeijingDuck.add(new Product("羊蝎子", 150.0, "滋补的羊蝎子", "https://img.icons8.com/doodle/48/goat.png"));
        productsBeijingDuck.add(new Product("炒肝", 60.0, "美味的炒肝", "https://img.icons8.com/doodle/48/liver.png"));
        productsBeijingDuck.add(new Product("爆肚", 55.0, "脆嫩的爆肚", "https://img.icons8.com/doodle/48/bowls.png"));
        productsBeijingDuck.add(new Product("卤煮火烧", 70.0, "浓郁的卤煮火烧", "https://img.icons8.com/doodle/48/fire.png"));
        productsBeijingDuck.add(new Product("豆沙包", 10.0, "甜蜜的豆沙包", "https://img.icons8.com/doodle/48/bread.png"));
        productsBeijingDuck.add(new Product("糖葫芦", 12.0, "酸甜的糖葫芦", "https://img.icons8.com/doodle/48/skewer.png"));
        productsBeijingDuck.add(new Product("豌豆黄", 18.0, "细腻的豌豆黄", "https://img.icons8.com/doodle/48/pea.png"));

        List<Product> productsNanjingDuck = new ArrayList<>();
        productsNanjingDuck.add(new Product("烤鸭", 100.0, "南京特色的烤鸭", "https://img.icons8.com/doodle/48/beijing-duck.png"));
        productsNanjingDuck.add(new Product("盐水鸭", 80.0, "鲜美的盐水鸭", "https://img.icons8.com/doodle/48/duck.png"));
        productsNanjingDuck.add(new Product("鸭血粉丝汤", 30.0, "浓郁的鸭血粉丝汤", "https://img.icons8.com/doodle/48/soup-bowl.png"));
        productsNanjingDuck.add(new Product("小笼包", 25.0, "鲜美的小笼包", "https://img.icons8.com/doodle/48/bread.png"));
        productsNanjingDuck.add(new Product("鸭油烧饼", 20.0, "酥脆的鸭油烧饼", "https://img.icons8.com/doodle/48/bread.png"));
        productsNanjingDuck.add(new Product("鸭舌", 40.0, "香脆的鸭舌", "https://img.icons8.com/doodle/48/tongue.png"));
        productsNanjingDuck.add(new Product("鸭掌", 50.0, "美味的鸭掌", "https://img.icons8.com/doodle/48/palm.png"));
        productsNanjingDuck.add(new Product("鸭翅", 60.0, "酥脆的鸭翅", "https://img.icons8.com/doodle/48/wing.png"));
        productsNanjingDuck.add(new Product("鸭肉粥", 45.0, "滋补的鸭肉粥", "https://img.icons8.com/doodle/48/soup-bowl.png"));
        productsNanjingDuck.add(new Product("鸭蛋", 15.0, "新鲜的鸭蛋", "https://img.icons8.com/doodle/48/egg.png"));
        productsNanjingDuck.add(new Product("鸭肉串", 35.0, "美味的鸭肉串", "https://img.icons8.com/doodle/48/skewer.png"));
        productsNanjingDuck.add(new Product("鸭肉包", 28.0, "鲜美的鸭肉包", "https://img.icons8.com/doodle/48/bread.png"));
        productsNanjingDuck.add(new Product("鸭肉丸", 30.0, "弹牙的鸭肉丸", "https://img.icons8.com/doodle/48/meatball.png"));
        productsNanjingDuck.add(new Product("鸭肉饺子", 40.0, "鲜美的鸭肉饺子", "https://img.icons8.com/doodle/48/dumplings.png"));
        productsNanjingDuck.add(new Product("鸭肉汤圆", 32.0, "Q弹的鸭肉汤圆", "https://img.icons8.com/doodle/48/tangyuan.png"));

        List<Product> productsShaanxiDuck = new ArrayList<>();
        productsShaanxiDuck.add(new Product("烤鸭", 90.0, "陕西风味的烤鸭", "https://img.icons8.com/doodle/48/beijing-duck.png"));
        productsShaanxiDuck.add(new Product("羊肉泡馍", 50.0, "传统的羊肉泡馍", "https://img.icons8.com/doodle/48/bread.png"));
        productsShaanxiDuck.add(new Product("凉皮", 20.0, "爽滑的凉皮", "https://img.icons8.com/doodle/48/noodles.png"));

        List<Product> productsHubeiDuck = new ArrayList<>();
        productsHubeiDuck.add(new Product("烤鸭", 80.0, "湖北风味的烤鸭", "https://img.icons8.com/doodle/48/beijing-duck.png"));
        productsHubeiDuck.add(new Product("热干面", 25.0, "经典的热干面", "https://img.icons8.com/doodle/48/noodles.png"));
        productsHubeiDuck.add(new Product("豆皮", 20.0, "香脆的豆皮", "https://img.icons8.com/doodle/48/bean-curd.png"));

        List<Product> productsAnhuiDuck = new ArrayList<>();
        productsAnhuiDuck.add(new Product("烤鸭", 70.0, "安徽风味的烤鸭", "https://img.icons8.com/doodle/48/beijing-duck.png"));
        productsAnhuiDuck.add(new Product("毛豆腐", 20.0, "香脆的毛豆腐", "https://img.icons8.com/doodle/48/tofu.png"));
        productsAnhuiDuck.add(new Product("臭鳜鱼", 80.0, "鲜美的臭鳜鱼", "https://img.icons8.com/doodle/48/fish.png"));

        List<Product> productsQuzhouDuckNeck = new ArrayList<>();
        productsQuzhouDuckNeck.add(new Product("鸭脖", 30.0, "脆嫩可口的鸭脖", "https://img.icons8.com/doodle/48/duck-neck.png"));
        productsQuzhouDuckNeck.add(new Product("鸭掌", 40.0, "美味的鸭掌", "https://img.icons8.com/doodle/48/palm.png"));
        productsQuzhouDuckNeck.add(new Product("鸭翅", 50.0, "酥脆的鸭翅", "https://img.icons8.com/doodle/48/wing.png"));

        List<Product> productsHunanStinkyTofu = new ArrayList<>();
        productsHunanStinkyTofu.add(new Product("臭豆腐", 20.0, "湖南特色的臭豆腐", "https://img.icons8.com/doodle/48/stinky-tofu.png"));
        productsHunanStinkyTofu.add(new Product("剁椒鱼头", 80.0, "鲜辣的剁椒鱼头", "https://img.icons8.com/doodle/48/fish-head.png"));
        productsHunanStinkyTofu.add(new Product("糖油粑粑", 15.0, "香甜的糖油粑粑", "https://img.icons8.com/doodle/48/dessert.png"));

        List<Product> productsNanjingStinkyTofu = new ArrayList<>();
        productsNanjingStinkyTofu.add(new Product("臭豆腐", 25.0, "南京风味的臭豆腐", "https://img.icons8.com/doodle/48/stinky-tofu.png"));
        productsNanjingStinkyTofu.add(new Product("鸭血粉丝汤", 30.0, "浓郁的鸭血粉丝汤", "https://img.icons8.com/doodle/48/soup-bowl.png"));
        productsNanjingStinkyTofu.add(new Product("盐水鸭", 80.0, "鲜美的盐水鸭", "https://img.icons8.com/doodle/48/duck.png"));

        // 将产品列表添加到对应的商户对象中
        merchantList.add(new Merchant("北京烤鸭", "专做烤鸭30年", "https://s1.aigei.com/src/img/png/62/62b31010f1d04a89a99c67bd9aa99903.png?imageMogr2/auto-orient/thumbnail/!282x282r/gravity/Center/crop/282x282/quality/85/%7CimageView2/2/w/282&e=1735488000&token=P7S2Xpzfz11vAkASLTkfHN7Fw-oOZBecqeJaxypL:r_VdXpns0_c2WreKOOxwOu5Cqzs=", productsBeijingDuck));
        merchantList.add(new Merchant("南京烤鸭", "专注做鸭20年", "https://s1.aigei.com/src/img/png/86/86690e7b479e4cb0956d3afac665524e.png?imageMogr2/auto-orient/thumbnail/!282x282r/gravity/Center/crop/282x282/quality/85/%7CimageView2/2/w/282&e=1735488000&token=P7S2Xpzfz11vAkASLTkfHN7Fw-oOZBecqeJaxypL:YaVUfbEnorJZKJA2HkQlY6URSEI=", productsNanjingDuck));
        merchantList.add(new Merchant("陕西烤鸭", "一直做烤鸭", "https://s1.4sai.com/src/img/png/c5/c5d21951dd1548f981df269048717208.png?imageMogr2/auto-orient/thumbnail/!282x282r/gravity/Center/crop/282x282/quality/85/%7CimageView2/2/w/282&e=1735488000&token=1srnZGLKZ0Aqlz6dk7yF4SkiYf4eP-YrEOdM1sob:H2PGKI4mR2ivRkRVdyVqMmYpc1o=", productsShaanxiDuck));
        merchantList.add(new Merchant("湖北烤鸭", "只会烤鸭", "https://img.icons8.com/doodle/48/ifood.png", productsHubeiDuck));
        merchantList.add(new Merchant("安徽烤鸭", "一个被烤鸭耽误的门店", "https://img.icons8.com/doodle/48/ifood.png", productsAnhuiDuck));
        merchantList.add(new Merchant("衢州鸭脖", "这是另一个商户", "https://img.icons8.com/doodle/48/ifood.png", productsQuzhouDuckNeck));
        merchantList.add(new Merchant("湖南臭豆腐", "这是另一个商户", "https://img.icons8.com/doodle/48/ifood.png", productsHunanStinkyTofu));
        merchantList.add(new Merchant("南京臭豆腐", "这是另一个商户", "https://img.icons8.com/doodle/48/ifood.png", productsNanjingStinkyTofu));

        // 初始化并设置MerchantAdapter
        merchantAdapter = new MerchantAdapter(merchantList, getContext(), this);
        recyclerView.setAdapter(merchantAdapter);

        // 设置搜索栏的文本变化监听器
        EditText searchBar = view.findViewById(R.id.search_bar);
        searchBar.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                filter(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {}
        });

        return view;
    }

    /**
     * 根据输入文本过滤商户列表
     * @param text 输入的搜索文本
     */
    public void filter(String text) {
        List<Merchant> filteredList = new ArrayList<>();
        for (Merchant merchant : merchantList) {
            if (merchant.getName().toLowerCase().contains(text.toLowerCase())) {
                filteredList.add(merchant);
            }
        }
        merchantAdapter.filterList(filteredList);
    }

    /**
     * 当商户项被点击时的处理方法
     * @param merchant 被点击的商户对象
     */
    @Override
    public void onItemClick(Merchant merchant) {
        Intent intent = new Intent(getContext(), MerchantDetailActivity.class);
        intent.putExtra("merchant", merchant);
        startActivity(intent);

        Toast.makeText(requireContext(), "点击了" + merchant.getName(), Toast.LENGTH_SHORT).show();
    }
}
