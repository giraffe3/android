package com.example.reacteui.Fragment;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.reacteui.Adapter.ActivityAdapter;
import com.example.reacteui.Adapter.MerchantAdapter;
import com.example.reacteui.DiscoverActivity.Activity;
import com.example.reacteui.DiscoverActivity.Merchant;
import com.example.reacteui.R;
import com.example.reacteui.Merchant.MerchantDetailActivity;

import java.util.ArrayList;
import java.util.List;

public class FragmentDiscoverPage extends Fragment implements MerchantAdapter.OnItemClickListener {

    private RecyclerView recyclerViewActivities;
    private RecyclerView recyclerViewMerchants;
    private ActivityAdapter activityAdapter;
    private MerchantAdapter merchantAdapter;
    private List<Activity> activityList;
    private List<Merchant> merchantList;

    public FragmentDiscoverPage() {
        // Required empty public constructor
    }

    public static FragmentDiscoverPage newInstance() {
        FragmentDiscoverPage fragment = new FragmentDiscoverPage();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            // Handle arguments if needed
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_discover_page, container, false);

        // Initialize RecyclerView for activities
        recyclerViewActivities = view.findViewById(R.id.recycler_view_activities);
        recyclerViewActivities.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        recyclerViewActivities.setHasFixedSize(true);

        // Initialize RecyclerView for merchants
        recyclerViewMerchants = view.findViewById(R.id.recycler_view_merchants);
        recyclerViewMerchants.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerViewMerchants.setHasFixedSize(true);

        // Initialize data lists
        activityList = new ArrayList<>();
        merchantList = new ArrayList<>();

        // Add sample data
        activityList.add(new Activity("5折大促", "全场5折优惠", "https://example.com/activity1.jpg"));
        activityList.add(new Activity("美食节", "各种美食优惠", "https://example.com/activity2.jpg"));
        activityList.add(new Activity("会员日", "会员专享优惠", "https://example.com/activity3.jpg"));

        merchantList.add(new Merchant("北京烤鸭", "专做烤鸭30年", "https://example.com/merchant1.jpg"));
        merchantList.add(new Merchant("南京烤鸭", "专注做鸭20年", "https://example.com/merchant2.jpg"));
        merchantList.add(new Merchant("陕西烤鸭", "一直做烤鸭", "https://example.com/merchant3.jpg"));

        // Initialize adapters
        activityAdapter = new ActivityAdapter(activityList, getContext());
        merchantAdapter = new MerchantAdapter(merchantList, getContext(), this);

        // Set adapters
        recyclerViewActivities.setAdapter(activityAdapter);
        recyclerViewMerchants.setAdapter(merchantAdapter);

        return view;
    }

    @Override
    public void onItemClick(Merchant merchant) {
        Log.d("FragmentDiscoverPage", "Clicked merchant: " + merchant.getName());
        Intent intent = new Intent(getContext(), MerchantDetailActivity.class);
        intent.putExtra("merchant", merchant);
        startActivity(intent);
    }
}
