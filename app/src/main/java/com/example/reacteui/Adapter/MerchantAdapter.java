// MerchantAdapter.java
package com.example.reacteui.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.reacteui.DiscoverActivity.Merchant;
import com.example.reacteui.R;

import java.util.List;

public class MerchantAdapter extends RecyclerView.Adapter<MerchantAdapter.MerchantViewHolder> {

    private List<Merchant> merchantList;
    private Context context;
    private OnItemClickListener listener;

    public interface OnItemClickListener {
        void onItemClick(Merchant merchant);
    }

    public MerchantAdapter(List<Merchant> merchantList, Context context, OnItemClickListener listener) {
        this.merchantList = merchantList;
        this.context = context;
        this.listener = listener;
    }

    @NonNull
    @Override
    public MerchantViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_merchant_discover, parent, false);
        return new MerchantViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MerchantViewHolder holder, int position) {
        Merchant merchant = merchantList.get(position);
        holder.textViewName.setText(merchant.getName());
        holder.textViewDescription.setText(merchant.getDescription());
        Glide.with(context).load(merchant.getImageUrl()).into(holder.imageView);

        holder.itemView.setOnClickListener(v -> {
            if (listener != null) {
                listener.onItemClick(merchant);
            }
        });
    }

    @Override
    public int getItemCount() {
        return merchantList.size();
    }

    public void filterList(List<Merchant> filteredList) {
        merchantList = filteredList;
        notifyDataSetChanged();
    }

    static class MerchantViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView textViewName;
        TextView textViewDescription;

        public MerchantViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.image_view_merchant);
            textViewName = itemView.findViewById(R.id.text_view_name);
            textViewDescription = itemView.findViewById(R.id.text_view_description);
        }
    }
}
