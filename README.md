# AndroidStudio大作业仿美团外卖

#### 介绍
{**以下是 Gitee 平台说明，您可以替换此简介**
Gitee 是 OSCHINA 推出的基于 Git 的代码托管平台（同时支持 SVN）。专为开发者提供稳定、高效、安全的云端软件开发协作平台
无论是个人、团队、或是企业，都能够用 Gitee 实现代码托管、项目管理、协作开发。企业项目请看 [https://gitee.com/enterprises](https://gitee.com/enterprises)}

#### 软件架构
软件架构说明
没有架构，纯靠想到哪写到哪
主要是想用一下viewpager和fragment的结合



#### 安装教程

1.  没有啥的其他安装包，有glide包，其他都自行添加
2.  只有简单的登录，没有登录就无法进入主页，有注册，没有密码修改，没有订单管理，没有收藏，没有地址管理，没有优惠券，没有评价，没有退款，没有分享，没有设置，没有关于我们
3.  此内容仅供学习交流使用，请勿用于商业用途。
4.  数据库就在 data.db 文件夹下
5.  写的不好仅供自己或者不怕的同学使用，只为了完成一个小小的任务罢了。哈哈哈哈哈哈
6.  项目截图在主文件夹下的img中
7.  再说一遍奥，写的真的是为了完成任务，各位大佬别骂，本人Java和Android都是速成课选手。能力有限。辅助工具《通义灵码》
8. 

#### 使用说明



#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
